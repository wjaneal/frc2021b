// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.AnalogGyro;
import edu.wpi.first.wpilibj.ADXRS450_Gyro;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import static edu.wpi.first.wpilibj.DoubleSolenoid.Value.*;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.SPI;
/**
 * The VM is configured to automatically run this class, and to call the functions corresponding to
 * each mode, as described in the TimedRobot documentation. If you change the name of this class or
 * the package after creating this project, you must also update the build.gradle file in the
 * project.
 */
public class Robot extends TimedRobot {
  public final WPI_TalonSRX m_RightMotor2 = new WPI_TalonSRX(4);
  public final WPI_TalonSRX m_RightMotor1 = new WPI_TalonSRX(3);
  public final WPI_TalonSRX m_LeftMotor2 = new WPI_TalonSRX(2);
  public final WPI_TalonSRX m_LeftMotor1 = new WPI_TalonSRX(1);
  public final WPI_VictorSPX shooter1 = new WPI_VictorSPX(5);
  public final WPI_VictorSPX shooter2 = new WPI_VictorSPX(6);
  public final WPI_VictorSPX collector = new WPI_VictorSPX(7);
  public int count = 0;
  public int time1 = 2000;
  private static final String kDefaultAuto = "Default";
  private static final String kCustomAuto = "My Auto";
  private static final String teststage1 = "Test Stage 1";
  private String m_autoSelected;
  private final SendableChooser<String> m_chooser = new SendableChooser<>();
  XboxController m_driveController = new XboxController(0);
  final SpeedControllerGroup m_motorGroupLeft = new SpeedControllerGroup(m_LeftMotor1, m_LeftMotor2);
  final SpeedControllerGroup m_motorGroupRight = new SpeedControllerGroup(m_RightMotor1, m_RightMotor2);
  final DifferentialDrive m_motorDiffDriveMain = new DifferentialDrive(m_motorGroupLeft, m_motorGroupRight);
  DoubleSolenoid exampleDouble = new DoubleSolenoid(1, 2);
  DoubleSolenoid anotherDoubleSolenoid = new DoubleSolenoid(3,4);
  //AnalogGyro Gyro = new AnalogGyro(0);
  ADXRS450_Gyro Gyro = new ADXRS450_Gyro(SPI.Port.kOnboardCS0);
  Encoder Encoder1 = new Encoder(0,1);
  Encoder Encoder2 = new Encoder(2,3);
 
  double x = 0;
  double y = 0;
  //****  Define a double for the centre line
  double CL = 250;
  double tolerance = 5;
  double OH = 100;
  double OW = 80;
  double H = 0;
  double W = 0;
  double t = 0;
  double d = 0;
  double ec1 = 0;
  double ec2 = 0;
  //double Gyro = 0;


  //**** set a variable for autonomous power: 0.3 */
  double power = 0.3;

  NetworkTableInstance inst = NetworkTableInstance.getDefault();
    NetworkTable table = inst.getTable("SmartDashboard");
    NetworkTableEntry xEntry=table.getEntry("X");
    NetworkTableEntry yEntry=table.getEntry("Y");
  /**
   * This function is run when the robot is first started up and should be used for any
   * initialization code.
   */
  @Override
  public void robotInit() {
    m_chooser.setDefaultOption("Default Auto", kDefaultAuto);
    m_chooser.addOption("My Auto", kCustomAuto);
    SmartDashboard.putData("Auto choices", m_chooser);
    Gyro.reset();
    Encoder1.reset();
    Encoder2.reset();
  }

  /**
   * This function is called every robot packet, no matter the mode. Use this for items like
   * diagnostics that you want ran during disabled, autonomous, teleoperated and test.
   *
   * <p>This runs after the mode specific periodic functions, but before LiveWindow and
   * SmartDashboard integrated updating.
   */
  @Override
  public void robotPeriodic() {}

  /**
   * This autonomous (along with the chooser code above) shows how to select between different
   * autonomous modes using the dashboard. The sendable chooser code works with the Java
   * SmartDashboard. If you prefer the LabVIEW Dashboard, remove all of the chooser code and
   * uncomment the getString line to get the auto name from the text box below the Gyro
   *
   * <p>You can add additional auto modes by adding additional comparisons to the switch structure
   * below with additional strings. If using the SendableChooser make sure to add them to the
   * chooser code above as well.
   */
  @Override
  public void autonomousInit() {
    m_autoSelected = m_chooser.getSelected();
    // m_autoSelected = SmartDashboard.getString("Auto Selector", kDefaultAuto);
    System.out.println("Auto selected: " + m_autoSelected);
    Gyro.reset();
    Encoder1.reset();
    Encoder2.reset();
  } 


  /** This function is called periodically during autonomous. */
  @Override
  public void autonomousPeriodic() {
    switch (m_autoSelected) {
      case kCustomAuto:
        // Put custom auto code here
        break;
      case kDefaultAuto:
      /*x = xEntry.getDouble(0.0);
      default:
      d=0.0;
      if (x < 250) {
        t = 0.5;}
      else {
        t = -0.5;
      }
      
      //t = 0.9;
      m_motorDiffDriveMain.arcadeDrive(d,t);
        // Put default auto code here
        break;*/
      /*if (Encoder1.getDistance() > 500 && Encoder2.getDistance() < -500) {
  d = 0;
}
else {
  d = 0.5;
  //Encoder1.reset();
  //Encoder2.reset();
}

/*if (Encoder1.getDistance() < 1000 && Encoder2.getDistance() < 1000) {
  t = 1;
}
else {
  t = 0;
  Encoder2.reset();
}*/

/*if (Gyro.getAngle() < 180) {
  t = 0.5;
}
else {
  t = 0;
}
if (m_driveController.getRawButton(4) ==true) {  
}
else{
//Reset the Gyro
//Gyro.reset();
  }
  m_motorDiffDriveMain.arcadeDrive(d,t);*/
  SmartDashboard.putNumber("ec1", Encoder1.getDistance());
  SmartDashboard.putNumber("ec2", Encoder2.getDistance());
  SmartDashboard.putNumber("Gyro", Gyro.getAngle());
  

  case teststage1:
 
  if (Encoder1.getDistance() < 1000 && Encoder2.getDistance() > -1000) {
    d = 0.9;
  }

  else{
    d = 0;
  }
  m_motorDiffDriveMain.arcadeDrive(d,t);

      
    }}
  

  /** This function is called once when teleop is enabled. */
  @Override
  public void teleopInit() {}

  /** This function is called periodically during operator control. */
  @Override
  public void teleopPeriodic() {}

  /** This function is called once when the robot is disabled. */
  @Override
  public void disabledInit() {}

  /** This function is called periodically when disabled. */
  @Override
  public void disabledPeriodic() {}

  /** This function is called once when test mode is enabled. */
  @Override
  public void testInit() {}

  /** This function is called periodically during test mode. */
  @Override
  public void testPeriodic() {}
}
