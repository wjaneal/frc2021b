// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.AnalogGyro;
import edu.wpi.first.wpilibj.ADXRS450_Gyro;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import static edu.wpi.first.wpilibj.DoubleSolenoid.Value.*;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.SPI;
import edu.wpi.first.wpilibj.Timer;
/**
 * The VM is configured to automatically run this class, and to call the functions corresponding to
 * each mode, as described in the TimedRobot documentation. If you change the name of this class or
 * the package after creating this project, you must also update the build.gradle file in the
 * project.
 */
public class Robot extends TimedRobot {
  public final WPI_TalonSRX m_RightMotor2 = new WPI_TalonSRX(4);
  public final WPI_TalonSRX m_RightMotor1 = new WPI_TalonSRX(3);
  public final WPI_TalonSRX m_LeftMotor2 = new WPI_TalonSRX(2);
  public final WPI_TalonSRX m_LeftMotor1 = new WPI_TalonSRX(1);
  public final WPI_VictorSPX shooter1 = new WPI_VictorSPX(5);
  public final WPI_VictorSPX shooter2 = new WPI_VictorSPX(6);
  public final WPI_VictorSPX collector = new WPI_VictorSPX(7);
  public int count = 0;
  public int time1 = 2000;
  private static final String kDefaultAuto = "Default";
  private static final String kCustomAuto = "My Auto";
  private static final String CstageAA0 = "Collect Stage AA 0";
  private static final String CstageAA1 = "Collect Stage AA 1";
  private static final String CstageAA2 = "Collect Stage AA 2";
  private static final String CstageAA3 = "Collect Stage AA 3";
  private static final String CstageAA4 = "Collect Stage AA 4";
  private static final String CstageAA5 = "Collect Stage AA 5";
  private static final String CstageAA6 = "Collect Stage AA 6";
  private static final String CstageAA7 = "Collect Stage AA 7";
  private static final String CstageAA8 = "Collect Stage AA 8";
  private static final String CstageAA9 = "Collect Stage AA 9";
  private static final String CstageAA10 = "Collect Stage AA 10";

  private static final String CstageAB1 = "Collect Stage AB 1";
  private static final String CstageAB2 = "Collect Stage AB 2";
  private static final String CstageAB3 = "Collect Stage AB 3";
  private static final String CstageAB4 = "Collect Stage AB 4";
  private static final String CstageAB5 = "Collect Stage AB 5";
  private static final String CstageAB6 = "Collect Stage AB 6";
  private static final String CstageAB7 = "Collect Stage AB 7";
  private static final String CstageAB8 = "Collect Stage AB 8";
  private static final String CstageAB9 = "Collect Stage AB 9";
  private static final String CstageAB10 = "Collect Stage AB 10";

  private static final String done = "All Done";
  private String m_autoSelected;
  private final SendableChooser<String> m_chooser = new SendableChooser<>();
  XboxController m_driveController = new XboxController(0);
  final SpeedControllerGroup m_motorGroupLeft = new SpeedControllerGroup(m_LeftMotor1, m_LeftMotor2);
  final SpeedControllerGroup m_motorGroupRight = new SpeedControllerGroup(m_RightMotor1, m_RightMotor2);
  final DifferentialDrive m_motorDiffDriveMain = new DifferentialDrive(m_motorGroupLeft, m_motorGroupRight);
  DoubleSolenoid exampleDouble = new DoubleSolenoid(1, 2);
  DoubleSolenoid anotherDoubleSolenoid = new DoubleSolenoid(3,4);
  //AnalogGyro Gyro = new AnalogGyro(0);
  ADXRS450_Gyro Gyro = new ADXRS450_Gyro(SPI.Port.kOnboardCS0);
  Encoder Encoder1 = new Encoder(0,1);
  Encoder Encoder2 = new Encoder(2,3);
 
  double x = 0;
  double y = 0;
  //****  Define a double for the centre line
  double CL = 250;
  double tolerance = 5;
  double OH = 100;
  double OW = 80;
  double H = 0;
  double W = 0;
  double t = 0;
  double d = 0;
  double ec1 = 0;
  double ec2 = 0;
  //double Gyro = 0;
  double collectorValue = 0;
double dAA1 = 800; //First stage - straight distance
double dAA2 = 500; //Fourth stage - straight distance
double dAA3 = 350; // Seventh Stage - straight distance
double dAA4 = 100; // Tenth Stage - straight distance
double aAA0 = 19;
double aAA1 = -80; //Third Stage - angle to turn
double aAA2 = 73;//Sixth Stage - angle to turn
double aAA3 = -30; //Ninth Stage - angle to turn

double dAB1 = 100;
double dAB2 = 100;
double dAB3 = 100;
double dAB4 = 100;
double aAB1 = 30;
double aAB2 = -90;
double aAB3 = 80;

double speed = -0.6;
double collectorTime = 0.7;
double shooterTime = 1.0;
double tAdjust = 0.22;
Timer timer = new Timer();
double collectorSpeed = 1.0;
//**** set a variable for autonomous power: 0.3 */
double power = 0.3;

  NetworkTableInstance inst = NetworkTableInstance.getDefault();
    NetworkTable table = inst.getTable("SmartDashboard");
    NetworkTableEntry xEntry=table.getEntry("X");
    NetworkTableEntry yEntry=table.getEntry("Y");
  /**
   * This function is run when the robot is first started up and should be used for any
   * initialization code.
   */
  @Override
  public void robotInit() {
    m_chooser.setDefaultOption("Default Auto", kDefaultAuto);
    m_chooser.addOption("My Auto", kCustomAuto);
    m_chooser.addOption("Auto 1", CstageAA1);
    SmartDashboard.putData("Auto choices", m_chooser);
    Gyro.reset();
    Encoder1.reset();
    Encoder2.reset();
  }

  /**
   * This function is called every robot packet, no matter the mode. Use this for items like
   * diagnostics that you want ran during disabled, autonomous, teleoperated and test.
   *
   * <p>This runs after the mode specific periodic functions, but before LiveWindow and
   * SmartDashboard integrated updating.
   */
  @Override
  public void robotPeriodic() {}

  /**
   * This autonomous (along with the chooser code above) shows how to select between different
   * autonomous modes using the dashboard. The sendable chooser code works with the Java
   * SmartDashboard. If you prefer the LabVIEW Dashboard, remove all of the chooser code and
   * uncomment the getString line to get the auto name from the text box below the Gyro
   *
   * <p>You can add additional auto modes by adding additional comparisons to the switch structure
   * below with additional strings. If using the SendableChooser make sure to add them to the
   * chooser code above as well.
   */
  @Override
  public void autonomousInit() {
    m_autoSelected = m_chooser.getSelected();
    // m_autoSelected = SmartDashboard.getString("Auto Selector", kDefaultAuto);
    System.out.println("Auto selected: " + m_autoSelected);
    Gyro.reset();
    Encoder1.reset();
    Encoder2.reset();
  } 


  /** This function is called periodically during autonomous. */
  @Override
  public void autonomousPeriodic() {
    switch (m_autoSelected) {
      case kCustomAuto:
        // Put custom auto code here
        break;
      case kDefaultAuto:
      default:
        SmartDashboard.putNumber("ec1", Encoder1.getDistance());
  SmartDashboard.putNumber("ec2", Encoder2.getDistance());
  SmartDashboard.putNumber("Gyro", Gyro.getAngle());
  m_autoSelected = CstageAA0;
  break;

  case CstageAA0:
  if (Gyro.getAngle()<aAA0){
    t = 0.5;
    d = 0;
  }
  else{
  t = 0;
  d = 0;
  Gyro.reset();
  Encoder1.reset();
  Encoder2.reset();
  m_autoSelected = CstageAA1;
}
m_motorDiffDriveMain.arcadeDrive(d,t);

  break;
  case CstageAA1:
  SmartDashboard.putNumber("ec1", Encoder1.getDistance());
  SmartDashboard.putNumber("ec2", Encoder2.getDistance());
  SmartDashboard.putNumber("Gyro", Gyro.getAngle());
  if (Encoder1.getDistance() > -dAA1 && Encoder2.getDistance() < dAA1) {
    d = speed;
    t = tAdjust; //Adjustment for straight motion
   
  }
  else{
    d = 0;
    Gyro.reset();
    Encoder1.reset();
    Encoder2.reset();
    m_autoSelected = CstageAA2;
    timer.reset();
    timer.start();
  }
  m_motorDiffDriveMain.arcadeDrive(d,t);

break;
  case CstageAA2:

  if (timer.get()<collectorTime) {
    d = speed;
    t = tAdjust;
    collector.set(collectorSpeed);
  }
  else {
    t = 0;
    d = 0;
    Gyro.reset();
    Encoder1.reset();
    Encoder2.reset();
    m_autoSelected = CstageAA3;
    collector.set(0);
  }
  m_motorDiffDriveMain.arcadeDrive(d,t);
  break;
    
    
  case CstageAA3:
  if (Gyro.getAngle() > aAA1  ) {
    d = 0;
    t = -0.5;
  }
  else {
    t = 0;
    d = 0;
    Gyro.reset();
    Encoder1.reset();
    Encoder2.reset();
    m_autoSelected = CstageAA4;
  }
  m_motorDiffDriveMain.arcadeDrive(d,t);
break;
    
  case CstageAA4:
  if (Encoder1.getDistance() > -dAA2 && Encoder2.getDistance() < dAA2){
    d = speed;
    t = 0;
  }

  else{
    d = 0;
    Gyro.reset();
    Encoder1.reset();
    Encoder2.reset();
    m_autoSelected = CstageAA5;
    timer.reset();
  }
  m_motorDiffDriveMain.arcadeDrive(d,t);

  break;


  case CstageAA5:
  if (timer.get() < collectorTime) {
    t = 0.5;
    d = 0;
    collector.set(collectorSpeed);
  }
  else {
    t = 0;
    Gyro.reset();
    Encoder1.reset();
    Encoder2.reset();
    m_autoSelected = CstageAA6;
    collector.set(0);
  }
  m_motorDiffDriveMain.arcadeDrive(d,t);
  break;


  case CstageAA6:
  if (Gyro.getAngle() < aAA2) {
    t = 0.5;
  }
  else {
    t = 0;
    Gyro.reset();
    Encoder1.reset();
    Encoder2.reset();
    m_autoSelected = CstageAA7;
  }
  m_motorDiffDriveMain.arcadeDrive(d,t);
  break;
    
  
  case CstageAA7:
  if (Encoder1.getDistance() > -dAA3 && Encoder2.getDistance() < dAA3) {
    d = speed;
    t = 0;
  }
  
  else{
    d = 0;
    Gyro.reset();
    Encoder1.reset();
    Encoder2.reset();
    m_autoSelected = CstageAA8;
    timer.reset();
    
  }
  m_motorDiffDriveMain.arcadeDrive(d,t);
  break;
  
  case CstageAA8:
  if (timer.get()<collectorTime){
    d = speed;
    t = 0;
    collector.set(collectorSpeed);
  }
  else {
    t = 0;
    Gyro.reset();
    Encoder1.reset();
    Encoder2.reset();
    m_autoSelected = CstageAA9;
    collector.set(0);
  }
  m_motorDiffDriveMain.arcadeDrive(d,t);
  break;

  case CstageAA9:
  if(Gyro.getAngle()>aAA3){
    t = -0.5;
  }    
  else{t = 0;
  d = 0;
  Gyro.reset();
  Encoder1.reset();
  Encoder2.reset();
  m_autoSelected = CstageAA10;
}
m_motorDiffDriveMain.arcadeDrive(d,t);
break;
case CstageAA10: 
  if (Encoder1.getDistance() > -dAA4 && Encoder2.getDistance() < dAA4) {
    d = speed;
  }

  else{
    d = 0;
    m_autoSelected = done;
  }
  m_motorDiffDriveMain.arcadeDrive(d,t);
  break;
 
case CstageAB1:
SmartDashboard.putNumber("ec1", Encoder1.getDistance());
  SmartDashboard.putNumber("ec2", Encoder2.getDistance());
  SmartDashboard.putNumber("Gyro", Gyro.getAngle());
  if (Encoder1.getDistance() > -dAB1 && Encoder2.getDistance() < dAB1) {
    d = speed;
    t = tAdjust; //Adjustment for straight motion
   
  }
  else{
    d = 0;
    Gyro.reset();
    Encoder1.reset();
    Encoder2.reset();
    m_autoSelected = CstageAB2;
    timer.reset();
    timer.start();
  }
  m_motorDiffDriveMain.arcadeDrive(d,t);
  break;

case CstageAB2:
if (timer.get()<collectorTime){
  d = speed;
  t = 0;
  collector.set(collectorSpeed);
}
else {
  t = 0;
  Gyro.reset();
  Encoder1.reset();
  Encoder2.reset();
  m_autoSelected = CstageAB3;
  collector.set(0);
}
m_motorDiffDriveMain.arcadeDrive(d,t);
break;

case CstageAB3:
if(Gyro.getAngle()>aAB1){
  t = -0.5;
}    
else{t = 0;
d = 0;
Gyro.reset();
Encoder1.reset();
Encoder2.reset();
m_autoSelected = CstageAB4;
}
m_motorDiffDriveMain.arcadeDrive(d,t);
break;

case CstageAB4:
if (Encoder1.getDistance() > -dAB2 && Encoder2.getDistance() < dAB2) {
  d = speed;
  t = 0;
}

else{
  d = 0;
  Gyro.reset();
  Encoder1.reset();
  Encoder2.reset();
  m_autoSelected = CstageAB5;
  timer.reset();
  
}
m_motorDiffDriveMain.arcadeDrive(d,t);
break;

case CstageAB5:
if (timer.get()<collectorTime){
  d = speed;
  t = 0;
  collector.set(collectorSpeed);
}
else {
  t = 0;
  Gyro.reset();
  Encoder1.reset();
  Encoder2.reset();
  m_autoSelected = CstageAB6;
  collector.set(0);
}
m_motorDiffDriveMain.arcadeDrive(d,t);
break;

case CstageAB6:
if(Gyro.getAngle()>aAB2){
  t = -0.5;
}    
else{t = 0;
d = 0;
Gyro.reset();
Encoder1.reset();
Encoder2.reset();
m_autoSelected = CstageAB7;
}
m_motorDiffDriveMain.arcadeDrive(d,t);
break;

case CstageAB7:


}
}

  /** This function is called once when teleop is enabled. */
  @Override
  public void teleopInit() {}

  /** This function is called periodically during operator control. */
  @Override
  public void teleopPeriodic() {
    if (m_driveController.getRawButton(5)){
      collectorValue = -1.0;
    }
    else{
      collectorValue = 0.0;
    }
    
    collector.set(collectorValue);
    shooter1.set(m_driveController.getRawAxis(1));
    shooter2.set(m_driveController.getRawAxis(1));

    if (m_driveController.getRawAxis(3)==0 && m_driveController.getRawAxis(2)!=0){
    m_motorDiffDriveMain.arcadeDrive((m_driveController.getRawAxis(2)), (m_driveController.getRawAxis(4)));
    }
    else if (m_driveController.getRawAxis(3)!=0 && m_driveController.getRawAxis(2)==0){
    m_motorDiffDriveMain.arcadeDrive((m_driveController.getRawAxis(3)*-1), (m_driveController.getRawAxis(4)));
    }
    else{
      m_motorDiffDriveMain.arcadeDrive(0,m_driveController.getRawAxis(4));
    }
  }

  /** This function is called once when the robot is disabled. */
  @Override
  public void disabledInit() {}

  /** This function is called periodically when disabled. */
  @Override
  public void disabledPeriodic() {}

  /** This function is called once when test mode is enabled. */
  @Override
  public void testInit() {}

  /** This function is called periodically during test mode. */
  @Override
  public void testPeriodic() {}
}
