// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.AnalogGyro;
import edu.wpi.first.wpilibj.ADXRS450_Gyro;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import static edu.wpi.first.wpilibj.DoubleSolenoid.Value.*;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.SPI;
/**
 * The VM is configured to automatically run this class, and to call the functions corresponding to
 * each mode, as described in the TimedRobot documentation. If you change the name of this class or
 * the package after creating this project, you must also update the build.gradle file in the
 * project.
 */
public class Robot extends TimedRobot {
  public final WPI_TalonSRX m_RightMotor2 = new WPI_TalonSRX(4);
  public final WPI_TalonSRX m_RightMotor1 = new WPI_TalonSRX(3);
  public final WPI_TalonSRX m_LeftMotor2 = new WPI_TalonSRX(2);
  public final WPI_TalonSRX m_LeftMotor1 = new WPI_TalonSRX(1);
  public final WPI_VictorSPX shooter1 = new WPI_VictorSPX(5);
  public final WPI_VictorSPX shooter2 = new WPI_VictorSPX(6);
  public final WPI_VictorSPX collector = new WPI_VictorSPX(7);
  public int count = 0;
  public int time1 = 2000;
  private static final String kDefaultAuto = "Default";
  private static final String kCustomAuto = "My Auto";
  private static final String Bstage1 = "Barrel Stage 1";
  private static final String Bstage2 = "Barrel Stage 2";
  private static final String Bstage3 = "Barrel Stage 3";
  private static final String Bstage4 = "Barrel Stage 4";
  private static final String Bstage5 = "Barrel Stage 5";
  private static final String Bstage6 = "Barrel Stage 6";
  private static final String Bstage7 = "Barrel Stage 7";
  private static final String Bstage8 = "Barrel Stage 8";
  private static final String Bstage9 = "Barrel Stage 9";
  private static final String Sstage1 = "Slalom Stage 1";
  private static final String Sstage2 = "Slalom Stage 2";
  private static final String Sstage3 = "Slalom Stage 3";
  private static final String Sstage4 = "Slalom Stage 4";
  private static final String Sstage5 = "Slalom Stage 5";
  private static final String Sstage6 = "Slalom Stage 6";
  private static final String Sstage7 = "Slalom Stage 7";
  private static final String Sstage8 = "Slalom Stage 8";
  private static final String Sstage9 = "Slalom Stage 9";
  private static final String Sstage10 = "Slalom Stage 10";
  private static final String Sstage11 = "Slalom Stage 11";
  private static final String Sstage12 = "Slalom Stage 12";
  private static final String Bnstage1 = "Bounce Stage 1";
  private static final String Bnstage2 = "Bounce Stage 2";
  private static final String Bnstage3 = "Bounce Stage 3";
  private static final String Bnstage4 = "Bounce Stage 4";
  private static final String Bnstage5 = "Bounce Stage 5";
  private static final String Bnstage6 = "Bounce Stage 6";
  private static final String Bnstage7 = "Bounce Stage 7";
  private static final String Bnstage8 = "Bounce Stage 8";
  private static final String Bnstage9 = "Bounce Stage 9";
  private static final String Bnstage10 = "Bounce Stage 10";
  private static final String Bnstage11 = "Bounce Stage 11";
  private static final String Bnstage12 = "Bounce Stage 12";
  private static final String Bnstage13 = "Bounce Stage 13";
  private static final String Bnstage14 = "Bounce Stage 14";
  private static final String done = "All Done";
  private String m_autoSelected;
  private final SendableChooser<String> m_chooser = new SendableChooser<>();
  XboxController m_driveController = new XboxController(0);
  final SpeedControllerGroup m_motorGroupLeft = new SpeedControllerGroup(m_LeftMotor1, m_LeftMotor2);
  final SpeedControllerGroup m_motorGroupRight = new SpeedControllerGroup(m_RightMotor1, m_RightMotor2);
  final DifferentialDrive m_motorDiffDriveMain = new DifferentialDrive(m_motorGroupLeft, m_motorGroupRight);
  DoubleSolenoid exampleDouble = new DoubleSolenoid(1, 2);
  DoubleSolenoid anotherDoubleSolenoid = new DoubleSolenoid(3,4);
  //AnalogGyro Gyro = new AnalogGyro(0);
  ADXRS450_Gyro Gyro = new ADXRS450_Gyro(SPI.Port.kOnboardCS0);
  Encoder Encoder1 = new Encoder(0,1);
  Encoder Encoder2 = new Encoder(2,3);
 
  double x = 0;
  double y = 0;
  //****  Define a double for the centre line
  double CL = 250;
  double tolerance = 5;
  double OH = 100;
  double OW = 80;
  double H = 0;
  double W = 0;
  double t = 0;
  double d = 0;
  double ec1 = 0;
  double ec2 = 0;
  //double Gyro = 0;
  double collectorValue = 0;
double distance = 800;
double speed = -0.7;
double turn = 0.4;
double turnAdjust = 0.20;
double ninety = 68;
double clockwiseAdjust = 0.25;
double counterClockwiseAdjust = 0.05;
  //**** set a variable for autonomous power: 0.3 */
  double power = 0.3;

  NetworkTableInstance inst = NetworkTableInstance.getDefault();
    NetworkTable table = inst.getTable("SmartDashboard");
    NetworkTableEntry xEntry=table.getEntry("X");
    NetworkTableEntry yEntry=table.getEntry("Y");
  /**
   * This function is run when the robot is first started up and should be used for any
   * initialization code.
   */
  @Override
  public void robotInit() {
    m_chooser.setDefaultOption("Default Auto", kDefaultAuto);
    m_chooser.addOption("My Auto", kCustomAuto);
    SmartDashboard.putData("Auto choices", m_chooser);
    Gyro.reset();
    Encoder1.reset();
    Encoder2.reset();
  }

  /**
   * This function is called every robot packet, no matter the mode. Use this for items like
   * diagnostics that you want ran during disabled, autonomous, teleoperated and test.
   *
   * <p>This runs after the mode specific periodic functions, but before LiveWindow and
   * SmartDashboard integrated updating.
   */
  @Override
  public void robotPeriodic() {}

  /**
   * This autonomous (along with the chooser code above) shows how to select between different
   * autonomous modes using the dashboard. The sendable chooser code works with the Java
   * SmartDashboard. If you prefer the LabVIEW Dashboard, remove all of the chooser code and
   * uncomment the getString line to get the auto name from the text box below the Gyro
   *
   * <p>You can add additional auto modes by adding additional comparisons to the switch structure
   * below with additional strings. If using the SendableChooser make sure to add them to the
   * chooser code above as well.
   */
  @Override
  public void autonomousInit() {
    m_autoSelected = m_chooser.getSelected();
    m_autoSelected = Sstage1;
    // m_autoSelected = SmartDashboard.getString("Auto Selector", kDefaultAuto);
    System.out.println("Auto selected: " + m_autoSelected);
    Gyro.reset();
    Encoder1.reset();
    Encoder2.reset();
  } 


  /** This function is called periodically during autonomous. */
  @Override
  public void autonomousPeriodic() {
    switch (m_autoSelected) {
      case kCustomAuto:
        // Put custom auto code here
        break;
      case kDefaultAuto:
      default:
      m_autoSelected = Sstage1;
      // Put default auto code here
        break;
      
  case Sstage1: //Counter clockwise turn
  if (Gyro.getAngle() > -ninety) {
    d = speed;
    t = -turn+counterClockwiseAdjust;
  }
  else if (Gyro.getAngle() <= -ninety) {
    d = 0;
    t = 0;
    Gyro.reset();
    Encoder1.reset();
    Encoder2.reset();
    m_autoSelected = Sstage2;
  }
  m_motorDiffDriveMain.arcadeDrive(d,t);
break;

  case Sstage2: //Clockwise turn
  if (Gyro.getAngle() < ninety-10) {
    d = speed;
    t = turn+clockwiseAdjust;
  }
  else if (Gyro.getAngle() >= ninety-10) {
    d = 0;
    t = 0;
    Gyro.reset();
    Encoder1.reset();
    Encoder2.reset();
    m_autoSelected = Sstage3;
  }
  m_motorDiffDriveMain.arcadeDrive(d,t);
break;

  case Sstage3: //straight
 if (Encoder1.getDistance() > -distance && Encoder2.getDistance() < distance) {
    d = speed;
    t = turnAdjust;
  }

  else {
    t = 0;
    Gyro.reset();
    Encoder1.reset();
    Encoder2.reset();
    m_autoSelected = Sstage4;
  }
  m_motorDiffDriveMain.arcadeDrive(d,t);
break;
  case Sstage4: //Clockwise turn
  if (Gyro.getAngle() < ninety+5) {
    d = speed;
    t = turn+clockwiseAdjust;
  }
  else if (Gyro.getAngle() >= ninety+5) {
    d = 0;
    t = 0;
    Gyro.reset();
    Encoder1.reset();
    Encoder2.reset();
    m_autoSelected = Sstage5;
  }
  m_motorDiffDriveMain.arcadeDrive(d,t);
break;

  case Sstage5://Counter clockwise turn 
   if (Gyro.getAngle() > -4*ninety-55) {
    d = speed;
    t = -turn-counterClockwiseAdjust-0.06;
  }
  else {
    d = 0;
    t = 0;
    Gyro.reset();
    Encoder1.reset();
    Encoder2.reset();
    m_autoSelected = Sstage6;
  }
  m_motorDiffDriveMain.arcadeDrive(d,t);
break;
  case Sstage6://Clockwise turn
   if (Gyro.getAngle() < ninety) {
    d = speed;
    t = turn+clockwiseAdjust+0.02;
  }
  else if (Gyro.getAngle() >= ninety) {
    d = 0;
    t = 0;
    Gyro.reset();
    Encoder1.reset();
    Encoder2.reset();
    m_autoSelected = Sstage7;
  }
  m_motorDiffDriveMain.arcadeDrive(d,t);
break;

  case Sstage7://Straight
  if (Encoder1.getDistance() > -distance-150 && Encoder2.getDistance() < distance-150) {
    d = speed;
    turn = turnAdjust;
  }
  else {
    t = 0;
    Gyro.reset();
    Encoder1.reset();
    Encoder2.reset();
    m_autoSelected = Sstage8;
  }
  m_motorDiffDriveMain.arcadeDrive(d,t);
break;
case Sstage8:
if (Gyro.getAngle() < ninety) {
  d = speed;
  t = turn+clockwiseAdjust+0.1;
}
else if (Gyro.getAngle() >= ninety) {
  d = 0;
  t = 0;
  Gyro.reset();
  Encoder1.reset();
  Encoder2.reset();
  m_autoSelected = Sstage9;
}
m_motorDiffDriveMain.arcadeDrive(d,t);
break;


  case Sstage9://Counter clockwise turn
 if (Gyro.getAngle() > -ninety) {
    d = speed;
    t = -turn-counterClockwiseAdjust-0.1;
  }
  else if (Gyro.getAngle() <= -ninety) {
    d = 0;
    t = 0;
    Gyro.reset();
    Encoder1.reset();
    Encoder2.reset();
    m_autoSelected = done;
  }
  m_motorDiffDriveMain.arcadeDrive(d,t);
break;
    }
  }
 /*
  case Bnstage1: 
  if (Gyro.getAngle() == 0) {
    t = -0.5;
  }
  else if (Gyro.getAngle() <= -80) {
    t = 0;
    Gyro.reset();
    Encoder1.reset();
    Encoder2.reset();
    m_autoSelected = Bnstage2;
  }
  m_motorDiffDriveMain.arcadeDrive(d,t);

  case Bnstage2:
  if (Encoder1.getDistance() < 167 && Encoder2.getDistance() > -167) {
    d = 0.9;
  }
  else{
    d = 0;
    Gyro.reset();
    Encoder1.reset();
    Encoder2.reset();
    m_autoSelected = Bnstage3;
  }
  m_motorDiffDriveMain.arcadeDrive(d,t);

  case Bnstage3:
  if (Gyro.getAngle() == 0) {
    t = 0.5;
  }
  else if (Gyro.getAngle() >= 300) {
    t = 0;
    Gyro.reset();
    Encoder1.reset();
    Encoder2.reset();
    m_autoSelected = Bnstage4;
  }
  m_motorDiffDriveMain.arcadeDrive(d,t);

  case Bnstage4:
  if (Encoder1.getDistance() < 167 && Encoder2.getDistance() > -167) {
    d = 0.9;
  }
  else{
    d = 0;
    Gyro.reset();
    Encoder1.reset();
    Encoder2.reset();
    m_autoSelected = Bnstage5;
  }
  m_motorDiffDriveMain.arcadeDrive(d,t);

  case Bnstage5:
  d = 0.5;
  if (Gyro.getAngle() == 0) {
    t = -0.5;
  }
  else if (Gyro.getAngle() <= -180) {
    t = 0;
    Gyro.reset();
    Encoder1.reset();
    Encoder2.reset();
    m_autoSelected = Bnstage6;
  }
  m_motorDiffDriveMain.arcadeDrive(d,t);

  case Bnstage6:
  if (Encoder1.getDistance() < 250 && Encoder2.getDistance() > -250) {
    d = 0.9;
  }
  else{
    d = 0;
    Gyro.reset();
    Encoder1.reset();
    Encoder2.reset();
    m_autoSelected = Bnstage7;
  }
  m_motorDiffDriveMain.arcadeDrive(d,t);

  case Bnstage7:
  if (Gyro.getAngle() == 0) {
    t = 0.5;
  }
  else if (Gyro.getAngle() >= 360) {
    t = 0;
    Gyro.reset();
    Encoder1.reset();
    Encoder2.reset();
    m_autoSelected = Bnstage8;
  }
  m_motorDiffDriveMain.arcadeDrive(d,t);

  case Bnstage8:
  if (Encoder1.getDistance() < 250 && Encoder2.getDistance() > -250) {
    d = 0.9;
  }
  else{
    d = 0;
    Gyro.reset();
    Encoder1.reset();
    Encoder2.reset();
    m_autoSelected = Bnstage9;
  }
  m_motorDiffDriveMain.arcadeDrive(d,t);

  case Bnstage9:
  if (Gyro.getAngle() == 0) {
    t = -0.5;
  }
  else if (Gyro.getAngle() <= -90) {
    t = 0;
    Gyro.reset();
    Encoder1.reset();
    Encoder2.reset();
    m_autoSelected = Bnstage10;
  }
  m_motorDiffDriveMain.arcadeDrive(d,t);

  case Bnstage10:
  if (Encoder1.getDistance() < 83 && Encoder2.getDistance() > -83) {
    d = 0.9;
  }
  else{
    d = 0;
    Gyro.reset();
    Encoder1.reset();
    Encoder2.reset();
    m_autoSelected = Bnstage11;
  }
  m_motorDiffDriveMain.arcadeDrive(d,t);

  case Bnstage11:
  if (Gyro.getAngle() == 0) {
    t = -0.5;
  }
  else if (Gyro.getAngle() <= -90) {
    t = 0;
    Gyro.reset();
    Encoder1.reset();
    Encoder2.reset();
    m_autoSelected = Bnstage12;
  }
  m_motorDiffDriveMain.arcadeDrive(d,t);

  case Bnstage12:
  if (Encoder1.getDistance() < 250 && Encoder2.getDistance() > -250) {
    d = 0.9;
  }
  else{
    d = 0;
    Gyro.reset();
    Encoder1.reset();
    Encoder2.reset();
    m_autoSelected = Bnstage13;
  }
  m_motorDiffDriveMain.arcadeDrive(d,t);

  case Bnstage13:
  if (Gyro.getAngle() == 0) {
    t = 0.5;
  }
  else if (Gyro.getAngle() >= 300) {
    t = 0;
    Gyro.reset();
    Encoder1.reset();
    Encoder2.reset();
    m_autoSelected = Bnstage14;
  }
  m_motorDiffDriveMain.arcadeDrive(d,t);

  case Bnstage14:
  if (Encoder1.getDistance() < 167 && Encoder2.getDistance() > -167) {
    d = 0.9;
  }

  else{
    d = 0;
    m_autoSelected = done;
  }
  m_motorDiffDriveMain.arcadeDrive(d,t);

  case done:
  d = 0;
  t = 0;

    }}
  

  /** This function is called once when teleop is enabled. */
  @Override
  public void teleopInit() {}

  /** This function is called periodically during operator control. */
  @Override
  public void teleopPeriodic() {
    if (m_driveController.getRawButton(5)){
      collectorValue = -1.0;
    }
    else{
      collectorValue = 0.0;
    }
    
    //collector.set(collectorValue);
    //shooter1.set(m_driveController.getRawAxis(2));
    //shooter2.set(m_driveController.getRawAxis(2));

    if (m_driveController.getRawAxis(3)==0 && m_driveController.getRawAxis(2)!=0){
    m_motorDiffDriveMain.arcadeDrive((m_driveController.getRawAxis(2)), (m_driveController.getRawAxis(4)));
    }
    else if (m_driveController.getRawAxis(3)!=0 && m_driveController.getRawAxis(2)==0){
    m_motorDiffDriveMain.arcadeDrive((m_driveController.getRawAxis(3)*-1), (m_driveController.getRawAxis(4)));
    }
    else{
      m_motorDiffDriveMain.arcadeDrive(0,m_driveController.getRawAxis(4));
    }
  }

  /** This function is called once when the robot is disabled. */
  @Override
  public void disabledInit() {}

  /** This function is called periodically when disabled. */
  @Override
  public void disabledPeriodic() {}

  /** This function is called once when test mode is enabled. */
  @Override
  public void testInit() {}

  /** This function is called periodically during test mode. */
  @Override
  public void testPeriodic() {}
}
