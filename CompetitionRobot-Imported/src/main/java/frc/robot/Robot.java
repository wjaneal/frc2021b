/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.ADXRS450_Gyro;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import static edu.wpi.first.wpilibj.DoubleSolenoid.Value.*;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.Timer;
/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the TimedRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the build.gradle file in the
 * project.
 */

/*public class NetworkTablesDesktopClients {
  public static void main(String[] args){
    new NetworkTablesDesktopClient().run();
}*/

 public class Robot extends TimedRobot {
  public final WPI_TalonSRX m_RightMotor2 = new WPI_TalonSRX(4);
  public final WPI_TalonSRX m_RightMotor1 = new WPI_TalonSRX(3);
  public final WPI_TalonSRX m_LeftMotor2 = new WPI_TalonSRX(2);
  public final WPI_TalonSRX m_LeftMotor1 = new WPI_TalonSRX(1);
  public final WPI_VictorSPX shooter1 = new WPI_VictorSPX(5);
  public final WPI_VictorSPX shooter2 = new WPI_VictorSPX(6);
  public final WPI_VictorSPX collector = new WPI_VictorSPX(7);
  public int count = 0;
  public int time1 = 2000;
  private static final String kDefaultAuto = "Default";
  private static final String kCustomAuto = "My Auto";
  private String m_autoSelected;
  private final SendableChooser<String> m_chooser = new SendableChooser<>();
  XboxController m_driveController = new XboxController(0);
  final SpeedControllerGroup m_motorGroupLeft = new SpeedControllerGroup(m_LeftMotor1, m_LeftMotor2);
  final SpeedControllerGroup m_motorGroupRight = new SpeedControllerGroup(m_RightMotor1, m_RightMotor2);
  final DifferentialDrive m_motorDiffDriveMain = new DifferentialDrive(m_motorGroupLeft, m_motorGroupRight);
  DoubleSolenoid exampleDouble = new DoubleSolenoid(1, 2);
  DoubleSolenoid anotherDoubleSolenoid = new DoubleSolenoid(3,4);
  //ADXRS450_Gyro Gyro = new ADXRS450_Gyro();
  Encoder Encoder1 = new Encoder(0,1);
  Encoder Encoder2 = new Encoder(2,3);
 
  double x = 0;
  double y = 0;
  //****  Define a double for the centre line
  double CL = 250;
  double tolerance = 5;
  double OH = 100;
  double OW = 80;
  double H = 0;
  double W = 0;
  double t = 0;
  double d = 0;

  //**** set a variable for autonomous power: 0.3 */
  double power = 0.3;

  NetworkTableInstance inst = NetworkTableInstance.getDefault();
    NetworkTable table = inst.getTable("SmartDashboard");
    NetworkTableEntry xEntry=table.getEntry("X");
    NetworkTableEntry yEntry=table.getEntry("Y");
    private final Timer m_timer = new Timer();
  /**
   * This function is run when the robot is first started up and should be
   * used for any initialization code.
   */
  @Override
  public void robotInit() {
    m_chooser.setDefaultOption("Default Auto", kDefaultAuto);
    m_chooser.addOption("My Auto", kCustomAuto);
    //SmartDashboard.putData("Auto choices", m_chooser);
    inst.startClientTeam(6162);
    inst.startDSClient();
    //Gyro.reset();
    Encoder1.reset();
    Encoder2.reset();
  }
  /**
   * This function is called every robot packet, no matter the mode. Use
   * this for items like diagnostics that you want ran during disabled,
   * autonomous, teleoperated and test.
   *
   * <p>This runs after the mode specific periodic functions, but before
   * LiveWindow and SmartDashboard integrated updating.
   */
  @Override
  public void robotPeriodic() {   //DiffferentialDrive m_motorDiffDriveMain;
    m_motorDiffDriveMain.arcadeDrive(m_driveController.getY(), m_driveController.getX());

    //exampleDouble. set(kOff);
    //exampleDouble. set(kForward);
    //exampleDouble. set(kReverse);
    if (m_driveController. getRawButton(3) ==true){
      count++;
      shooter1. set(1); 
      shooter2. set(1);
      if (count < time1) {
      collector. set(0);
      }
      else {  
        collector. set(0.3);
      }
    }
    else{
      shooter1. set(0);
      shooter2. set(0);
      count = 0;
    }
    //Similar structure for the collector - button 2
    
    if (m_driveController.getRawButton(1) ==true && m_driveController.getRawButton(2) ==false) {
      collector. set(1);
    }
    else if (m_driveController.getRawButton(1) ==false && m_driveController.getRawButton(2) ==true) {
      collector.set(-0.3);
    }
    else {
      collector.set(0);
    }
double x = xEntry.getDouble(0.0); //****Copy these to autonomous
double y = yEntry.getDouble(0.0);
//SmartDashboard.putNumber("X",x);
//SmartDashboard.putNumber("Y",y);
 
    }
    
  /**
   * This autonomous (along with the chooser code above) shows how to select
   * between different autonomous modes using the dashboard. The sendable
   * chooser code works with the Java SmartDashboard. If you prefer the
   * LabVIEW Dashboard, remove all of the chooser code and uncomment the
   * getString line to get the auto name from the text box below the Gyro
   *
   * <p>You can add additional auto modes by adding additional comparisons to
   * the switch structure below with additional strings. If using the
   * SendableChooser make sure to add them to the chooser code above as well.
   */
  @Override
  public void autonomousInit() {
      m_timer.reset();
    m_timer.start();
//****Check the values of X and Y
double x = xEntry.getDouble(0.0); 
double y = yEntry.getDouble(0.0);
// If statement for rotation
/*if (x < CL + tolerance) {
  m_motorDiffDriveMain.arcadeDrive(0,0.9);
}
else if (x < CL - tolerance) {
  m_motorDiffDriveMain.arcadeDrive(0,-0.9);
}
else {
  m_motorDiffDriveMain.arcadeDrive(0,0);
}
double t = 0;
double d = 0;
m_motorDiffDriveMain.arcadeDrive(d,t);
// If statement for forward / backward motion
/*if (H > OH + tolerance) {
  m_motorDiffDriveMain.arcadeDrive(-0.3,0);
}
else if (H < OH - tolerance) {
  m_motorDiffDriveMain.arcadeDrive(0.3,0);
}
else {
  m_motorDiffDriveMain.arcadeDrive(0,0);
}

// Turn and drive together
double t = 0;
double d = 0;

if (x < CL + tolerance) {
  t = 0.3;
}
else if (x < CL - tolerance) {
  t = -0.3;
}
else {
  t = 0;
}

if (H > OH + tolerance) {
  d = -0.3;
}
else if (H < OH - tolerance) {
  d = 0.3;
}
else {
  d = 0;
}
m_motorDiffDriveMain.arcadeDrive(d,t);

double ec1 = 0;
double ec2 = 0;
double gyro = 0;

if (Encoder1.getDistance() < 1000 && Encoder2.getDistance() < 1000) {
  d = 1;
}
else {
  d = 0;
  Encoder1.reset();
}

if (Encoder1.getDistance() < 1000 && Encoder2.getDistance() < 1000) {
  t = 1;
}
else {
  t = 0;
  Encoder2.reset();
}

if (Gyro.getAngle() < 180) {
  t = 1;
}
else {
  t = 0;
}
if (m_driveController. getRawButton(4) ==true) {  
}
else{
//Reset the Gyro
Gyro.reset();
}*/
}

  /**
   * This function is called periodically during autonomous.
   */
  @Override
  public void autonomousPeriodic() {
     
     double x = xEntry.getDouble(0.0); 
    //double y = yEntry.getDouble(0.0);
//SmartDashboard.putNumber("x1", x);
//SmartDashboard.putNumber("c", CL);
//SmartDashboard.putNumber("t", tolerance);
/*
    if (x > CL + tolerance) {
      t = 0.9;
    }
    else if (x < CL - tolerance) {
      t = -0.9;
    }
    else {
      t = 0; 
    }*/
//d=0.0;
//t = 0.9;
//m_motorDiffDriveMain.arcadeDrive(d,t);
      if (m_timer.get() < 2.0) {
      m_motorDiffDriveMain.arcadeDrive(0.5, 0.0); // drive forwards half speed
    } else {
      m_motorDiffDriveMain.stopMotor(); // stop robo
  }
  }

  /**
   * This function is called periodically during operator control.
   */
  @Override
  public void teleopPeriodic() {
    xEntry.getDouble(x);
    yEntry.getDouble(y);
  }
     

  /**
   * This function is called periodically during test mode.
   */
  @Override
  public void testPeriodic() {
  }
}
